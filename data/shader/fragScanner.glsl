#version 120

varying vec3 normal0;

varying vec4 diffuse;
varying vec4 ambient;
varying vec4 specular;

varying vec3 half_vector;
varying vec3 ec_pos;

uniform float far_plane;
uniform float near_plane;
uniform mat4 ViewMatrix;
uniform mat4 LightModelViewMatrix;

uniform sampler2D diffuse_texture_color;
uniform sampler2D specular_texture_color;

float rand(vec2 co) {
  return fract(sin(dot(co.xy,vec2(12.9898,78.233))) * 43758.5453);
}

float abs_rand_0_mean_software(vec2 co) {
  return abs(rand(co) - 0.5);
}

void main() {
  
  const float GLOBAL_AMBIENT = 0.2;
  vec4 color = gl_FrontMaterial.emission;
  vec4 diffuse_color = diffuse * texture2D(diffuse_texture_color, gl_TexCoord[0].st);
  vec4 spec_color = specular * texture2D(specular_texture_color, gl_TexCoord[1].st);

  vec3 spot_dir = vec3(ViewMatrix * vec4(gl_LightSource[0].spotDirection, 0.0));
  vec3 light_dir = -ec_pos;

  float dist = length(light_dir);

  vec3 n = normalize(normal0);
  float n_dot_hv = max(dot(n, normalize(half_vector)), 0.0);

  color = GLOBAL_AMBIENT * ambient;

  if (n_dot_hv > 0.0) {

    float spot_effect = dot(normalize(spot_dir), normalize(light_dir));

    float att = 1.0 / (gl_LightSource[0].constantAttenuation + gl_LightSource[0].linearAttenuation*dist + gl_LightSource[0].quadraticAttenuation*dist*dist);

    color += att * (diffuse_color * n_dot_hv + ambient);
    color += att * spec_color * pow(n_dot_hv, gl_FrontMaterial.shininess);

    float corrected_dist = spot_effect * dist;
    float dist_ratio = 65536.0f * (corrected_dist - near_plane) / (far_plane - near_plane);
    color.g = floor(dist_ratio / 256.0f) / 256.0;
    color.b = mod(dist_ratio, 256.0f) / 256.0;
    color.a = 1.0;
  } 
  else {
    color = vec4(0.0,0.0,0.0,1.0);
  }

  gl_FragColor = color;
}
