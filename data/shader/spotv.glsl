#version 120

attribute vec3 position;
attribute vec2 diffuse_tex;
attribute vec2 specular_tex;
attribute vec3 normal;

uniform mat4 ViewMatrix;
uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ModelViewProjectionMatrix;

varying vec3 normal0;

varying vec4  diffuse;
varying vec4  ambient;
varying vec4  specular;
varying vec3  half_vector;
varying vec3  ec_pos;

void main() {
  specular = vec4(1.0, 1.0, 1.0, 1.0);

  normal0 = normalize(NormalMatrix * normal);

  // Get coordinates in camera frame.
  ec_pos = vec3(ModelViewMatrix * vec4(position, 1.0));
  vec3 ec_light_dir = vec3(ViewMatrix * vec4(gl_LightSource[0].spotDirection, 0.0));

  half_vector = normalize(ec_light_dir);

  gl_TexCoord[0].st = diffuse_tex;
  gl_TexCoord[1].st = specular_tex;

  diffuse  = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
  specular = gl_FrontMaterial.specular * gl_LightSource[0].specular;
  ambient  = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;

  gl_Position = ModelViewProjectionMatrix * vec4(position, 1.0);
}
