#include "header.h"

int main(int argc, char** argv) {
	std::experimental::filesystem::remove_all("vizu");
	std::experimental::filesystem::create_directory("vizu");

	remove("mesh.ply");

	model_filename = "../data/models/model.ply";

	if (!glfwInit()) {
		std::cerr << "Failed to initialize GLFW" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);

	GLFWwindow* window = glfwCreateWindow(width, height, "SCANNER", NULL, NULL);

	if (!window) {
		std::cerr << "Failed to open GLFW window." << std::endl;
		glfwTerminate();
		return -1;
	}

	//Open window
	glfwMakeContextCurrent(window);
	glfwSetCursorPosCallback(window, mouse_callback);

	if (glewInit() != GLEW_OK) {
		std::cerr << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Setup ImGui binding
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui_ImplGlfwGL3_Init(window, true);
	ImGui::StyleColorsLight();

	//Keypress
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	Scene scene(model_filename, false, 0.0f, glm::vec3(0.0f, 0.0f, -400.0f), 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, width, height);
	
	double current_time = glfwGetTime();
	float delta_time = current_time;

	do {

		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			camera.ProcessKeyboard(LEFT, delta_time);
		}

		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			camera.ProcessKeyboard(RIGHT, delta_time);
		}

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			camera.ProcessKeyboard(FORWARD, delta_time);
		}

		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			camera.ProcessKeyboard(BACKWARD, delta_time);
		}

		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			scene.TargetPointPosition.x += 2.5;
			pressed = true;
		}

		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			scene.TargetPointPosition.x -= 2.5;
			pressed = true;
		}

		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			scene.TargetPointPosition.y += 2.5;
			pressed = true;
		}

		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			scene.TargetPointPosition.y -= 2.5;
			pressed = true;
		}

		if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
			scene.TargetPointPosition.z += 2.5;
			pressed = true;
		}

		if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS) {
			scene.TargetPointPosition.z -= 2.5;
			pressed = true;
		}

		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
			camera.MouseRotation = !camera.MouseRotation;
		}

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS || glfwWindowShouldClose(window)) {
			saved_now_quit = true;
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glViewport(0, 0, width, height);
		scene.render(camera, true);

		if (ScanningStart) {

			for (int j = 0; j < ScanningPoints.size(); j++) {
				if (j != scene.PointsReached) continue;
				glm::vec3 EffectorPosition = kinematics.getEffectorPosition();
				float distance = kinematics.distance(EffectorPosition, ScanningPoints[scene.PointsReached]);

				if (!use_robot)
				{
					camera.ScannerPosition = ScanningPoints[scene.PointsReached];
				}

				if (use_robot && distance > 10.0f && scene.PointsReached < ScanningPoints.size()) {
					kinematics.inverseKinematics(camera, scene, ScanningPoints[scene.PointsReached]);
				}
				else {
					name = static_cast<int>(scene.PointsReached);
					nameS = std::to_string(name);
					name2 = static_cast<int>(scene.RotaReached - 1);
					nameS2 = std::to_string(name2);
					filenameTransformation = "scans//" + nameS + "p" + nameS2 + "r" + ".txt";
					fileStream.open(filenameTransformation);

					if (fileStream.fail()) {
						
						if (!camera.ScanningView && !camera.ScanningView)
						{
							glfwPollEvents();
							glfwSwapBuffers(window);
							glfwPollEvents();
							camera.ScanningView = true;
							scene.ScanningMode = true;
							glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
							glViewport(0, 0, width, height);
							scene.render(camera, true);
							changed = true;
						}

						scene.render(camera, true);

						scene.save_PCD_point_cloud(width, height, camera);
						CameraPosition.push_back(camera.Position);

						if (changed)
						{
							camera.ScanningView = false;
							scene.ScanningMode = false;
							changed = false;
						}

						filenameCameraPosition = "scans//" + nameS + "p" + nameS2 + "r" + "CameraPosition" + ".txt";
						std::ofstream out(filenameCameraPosition, std::ios_base::binary | std::ios_base::out);
						out << camera.Position.x << endl;
						out << camera.Position.y << endl;
						out << camera.Position.z << endl;

						scene.PointsReached++;
						ScansDone++;

						if (filenameTransformation == "scans//0p0r.txt") {
							registration = true;
							t0 = std::thread(&RECON::doNormalsParalel, recon, ScanningPoints.size(), Rotations);
						}
					}
				}
			}

			/*lastNameN = "normalsDone.txt";
			lastF.open(lastNameN);

			if (!lastF.fail() && !done && ScansDone == MaxScans) {
				printf("registration started\n");
				t1 = std::thread(&ICP::registerCloud, registerICP, ScanningPoints.size(), Rotations, CameraPosition);
				done = true;
				lastF.close();
				remove(lastNameN.c_str());
			}*/

			if (ScanningPoints.size() == scene.PointsReached && (scene.RotaReached - 1) != Rotations) {

				scene.ModelRotation += 1.0f;
				int scanningRotationInt = static_cast<int>(ScanningRotation);

				if (scene.ModelRotation == (scanningRotationInt * scene.RotaReached)) {

					scene.RotaReached++;
					scene.PointsReached = 0;
				}
			}
		}
		glLineWidth(5.0);

		glBegin(GL_LINE_LOOP);
		glColor3f(0.5f, 0.5f, 0.5f);
		glVertex2f(1.0, -0.5);
		glVertex2f(0.5, -0.5);
		glVertex2f(0.5, -1.0);
		glEnd();
		glFlush();

		glScissor(width * 0.75, 0, width * 0.25, height * 0.25);
		glEnable(GL_SCISSOR_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_SCISSOR_TEST);
		
		glViewport(width * 0.75, 0, width * 0.25, height * 0.25);		
		scene.render(cameraScanning, false);

		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			int i = 0, j = 0;
			OnePoint = false;
			bool reachable = true;
			float distanceTarget;
			float parts_len = 0;
			for (i = 0; i < ScanningPoints.size(); i++) {
				if ((ScanningPoints[i].x == scene.getTargetPosition().x && ScanningPoints[i].y == scene.getTargetPosition().y && ScanningPoints[i].z == scene.getTargetPosition().z)
					|| (ScanningPoints[i].x == camera.Position.x && ScanningPoints[i].y == camera.Position.y && ScanningPoints[i].z == camera.Position.z)) {
					OnePoint = true;
				}
			}

			std::vector<ROBOT*> robot_parts = ROBOT::getAllObjects();
			int robot_size = robot_parts.size();

			glm::vec3 length = glm::vec3(0.0f, 0.0f, 0.0f);
			glm::vec3 start = glm::vec3(0.0f, -27.0f, -700.0f);

			for (int i = 0; i < robot_size; i++)
			{
				length += robot_parts.at(i)->Lenght;
			}
			parts_len = std::abs(length.x) + std::abs(length.y) + std::abs(length.z);

			float tempx, tempy, tempz;
			tempx = (scene.getTargetPosition().x - start.x);
			tempx = tempx * tempx;
			tempy = (scene.getTargetPosition().y - start.y);
			tempy = tempy * tempy;
			tempz = (scene.getTargetPosition().z - start.z);
			tempz = tempz * tempz;
			distanceTarget = tempx + tempy + tempz;
			distanceTarget = sqrt(distanceTarget);

			if (parts_len-100 < distanceTarget && parts_len != 0 && distanceTarget != 0 && pressed)
			{
				reachable = false;
				printf("Point is too far, robot can not rach to this position.\n");
			}

			if (scene.getTargetPosition().y < 0 && !OnePoint && pressed)
			{
				reachable = false;
				printf("This point is too low, choose higher position.\n");
			}

			if (!OnePoint && !camera.MouseRotation && pressed) {
				if (reachable)
				{
					ScanningPoints.push_back(scene.getTargetPosition());
					Points++;
				}
				pressed = false;
			}
			if (!OnePoint && camera.MouseRotation)
			{
				ScanningPoints.push_back(camera.Position);
				Points++;
			}
		}

		glfwPollEvents();

		ImGui_ImplGlfwGL3_NewFrame();
		{
			ImGui::Text("(%.1f FPS)", ImGui::GetIO().Framerate);

			ImGui::Text("Press R for mouse rotation - %s", camera.MouseRotation ? "enabled" : "disabled");

			if (ImGui::Button("Scanner View")) {
				camera.ScanningView = !camera.ScanningView;
				scene.ScanningMode = !scene.ScanningMode;
			}

			ImGui::Checkbox("Use robot to reach points", &use_robot);

			ImGui::Text("\nScanning parameters\n");
      //rovnomerny noise
			ImGui::SliderInt("Gaussian noise", &scene.Noise, 0, 5); //standard deviation

      //distortion
      ImGui::Text("Radial distortion");
      ImGui::SliderFloat("K1", &scene.K1, 0, 0.05);
      ImGui::SliderFloat("K2", &scene.K2, 0, 0.05);
      ImGui::SliderFloat("K3", &scene.K3, 0, 0.05);
      ImGui::Text("Tangential distortion");
      ImGui::SliderFloat("P1", &scene.P1, 0, 0.05);
      ImGui::SliderFloat("P2", &scene.P2, 0, 0.05);
      
      ImGui::Checkbox("Statistical Outlier Removal", &recon.OutlierRemoval);
  
			ImGui::Text("Initial transformation\n");
			ImGui::RadioButton("Position & rotation", &scene.InitialTransformation, 0);
			ImGui::SameLine();
			ImGui::RadioButton("Keypoint matching", &scene.InitialTransformation, 1);
      ImGui::SameLine();
      ImGui::RadioButton("Both", &scene.InitialTransformation, 2);

			ImGui::SliderInt("Density", &scene.CloudDensity, 1, 5);

			ImGui::SliderFloat("Rotation (degrees)", &ScanningRotation, 0.0f, 360.0f);

			ImGui::InputInt("Rotations #", &Rotations);

      for (int i = 0; i < ScanningPoints.size(); i++) {
				ImGui::Text("Point %f %f %f", ScanningPoints[i].x, ScanningPoints[i].y, ScanningPoints[i].z);
			}

			MaxScans = ScanningPoints.size() * (Rotations + 1);

			if (ImGui::Button("Start scanning")) {
				std::remove("scans//00.pcd");
				ScanningStart = true;
			}

			ImGui::Text("Scans captured: %d/%d\n", ScansDone, MaxScans);

			if (ImGui::Button("Reconstruct")) {
				reconstruction = true;
				t0.join();
				recon.reconstruct();
			}
		}

		// Rendering
		ImGui::Render();
		ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
		glfwSwapBuffers(window);
		glfwPollEvents();

	} while (!saved_now_quit);

	if (registration && !reconstruction) {
		t0.join();
	}

	glfwTerminate();

	return 0;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (camera.MouseRotation) {

		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos;
		lastX = xpos;
		lastY = ypos;

		float sensitivity = 0.6f;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		camera.UpdateCamera(xoffset, yoffset);
	}
}
