#pragma once
#ifndef MESH_H
#define MESH_H
#define GLM_ENABLE_EXPERIMENTAL
#define INVALID_OGL_VALUE 0xFFFFFFFF
#define M_PI 3.14159265358979323846

#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/string_cast.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <flann/flann.hpp>
#include <flann/algorithms/kdtree_single_index.h>

#include "texture.h"

const size_t MAX_LEAF_SIZE = 16;
const float MIN_NEAR_PLANE = 0.01;

struct Vertex
{
	glm::vec3 pos;
	glm::vec2 diffuse_tex;
	glm::vec2 specular_tex;
	glm::vec3 normal;

	Vertex() {}

	Vertex(const glm::vec3& pos_, const glm::vec2& dtex_, const glm::vec2& stex_, const glm::vec3& normal_)
		: pos(pos_), diffuse_tex(dtex_), specular_tex(stex_), normal(normal_)
	{  }

	float x() const { return pos.x; }
	float y() const { return pos.y; }
	float z() const { return pos.z; }
};

class Mesh {
public:

	Mesh() : min_extremities(0.0f, 0.0f, 0.0f), max_extremities(0.0f, 0.0f, 0.0f) { }

	~Mesh() {
		clear();
	}

	glm::vec3 dimensions() const {
		return max_extremities - min_extremities;
	}

	bool load_mesh(const std::string& filename) {

		clear();

		bool ret = false;
		Assimp::Importer importer;

		const aiScene* scene = importer.ReadFile(filename.c_str(), aiProcess_Triangulate | aiProcess_GenNormals);

		if (scene)
			ret = init_from_scene(scene, filename);
		else
			std::cerr << "Error parsing '" << filename << "': " << importer.GetErrorString() << std::endl;

		return ret;
	}

	void render(Shader* shader_program) {

		shader_program->bind();

		GLuint position_loc = glGetAttribLocation(shader_program->id(), "position");
		GLuint diffuse_tex_loc = glGetAttribLocation(shader_program->id(), "diffuse_tex");
		GLuint specular_tex_loc = glGetAttribLocation(shader_program->id(), "specular_tex");
		GLuint normal_loc = glGetAttribLocation(shader_program->id(), "normal");

		glEnableVertexAttribArray(position_loc);
		glEnableVertexAttribArray(diffuse_tex_loc);
		glEnableVertexAttribArray(specular_tex_loc);
		glEnableVertexAttribArray(normal_loc);

		for (size_t i = 0; i < entries.size(); ++i) {
			glBindBuffer(GL_ARRAY_BUFFER, entries[i].vb);

			glVertexAttribPointer(position_loc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
			glVertexAttribPointer(diffuse_tex_loc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)12); // starts at 12 because 3 floats for position before 2 floats for normal
			glVertexAttribPointer(specular_tex_loc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)20);
			glVertexAttribPointer(normal_loc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)28); // makes room for 7 floats

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, entries[i].ib);

			const size_t material_index = entries[i].material_index;


			if (material_index < textures.size() && textures[material_index]) {
				textures[material_index]->bind(shader_program);
			}

			glDrawElements(GL_TRIANGLES, entries[i].num_indices, GL_UNSIGNED_INT, 0);
		}

		glDisableVertexAttribArray(position_loc);
		glDisableVertexAttribArray(diffuse_tex_loc);
		glDisableVertexAttribArray(specular_tex_loc);
		glDisableVertexAttribArray(normal_loc);

		shader_program->unbind();
	}

	glm::vec3 centroid() const {
		return centroid_;
	}

	float nearest_point(glm::vec4& p, glm::vec4& result) const {
		float d = entries[0].nearest_point(p, result);

		for (size_t i = 1; i < entries.size(); ++i) {
			glm::vec4 tmp_result;
			float tmp_distance;
			//tieto dva riadky rly not sure
			p.x = i;
			p.y = i;

			tmp_distance = entries[i].nearest_point(p, tmp_result);

			if (tmp_distance < d) {
				d = tmp_distance;
				result = tmp_result;
			}
		}

		return d;
	}

	float near_plane_bound(const glm::mat4& model_to_object_coords, glm::vec4& camera_pos) const {
		glm::vec4 nearest;

		nearest_point(camera_pos, nearest);
		nearest.w = 0.0;

		float distance, distanceA, distanceB, distanceC, beta, betaFinal, Va, tempx, tempy, tempz;

		tempx = (0.0 - camera_pos.x); //0.0
		tempx = tempx * tempx;
		tempy = (0.0 - camera_pos.y); //0.0
		tempy = tempy * tempy;
		tempz = (0.0 - camera_pos.z); //depth
		tempz = tempz * tempz;
		distanceA = tempx + tempy + tempz;
		distanceA = sqrt(distanceA);

		tempx = (nearest.x - camera_pos.x); //0.0
		tempx = tempx * tempx;
		tempy = (nearest.y - camera_pos.y); //0.0
		tempy = tempy * tempy;
		tempz = (nearest.z - camera_pos.z); //depth
		tempz = tempz * tempz;
		distanceB = tempx + tempy + tempz;
		distanceB = sqrt(distanceB);

		tempx = (nearest.x - 0.0); //0.0
		tempx = tempx * tempx;
		tempy = (nearest.y - 0.0); //0.0
		tempy = tempy * tempy;
		tempz = (nearest.z - 0.0); //depth
		tempz = tempz * tempz;
		distanceC = tempx + tempy + tempz;
		distanceC = sqrt(distanceC);

		beta = ((distanceA*distanceA) + (distanceC * distanceC) - (distanceB * distanceB)) / (2 * distanceA*distanceC);
		betaFinal = acos(beta) * 180.0 / M_PI;

		Va = distanceC * (sin(betaFinal*M_PI / 180.0));

		distance = distanceB * distanceB - Va * Va;
		distance = sqrt(distance);

		return distance;
	}

	float far_plane_bound(const glm::mat4& model_to_object_coords, const glm::vec4& camera_pos) const {

		glm::vec4 negative_camera_pos(-camera_pos);
		glm::vec4 farthest;
		nearest_point(negative_camera_pos, farthest);
		farthest.w = 0.0;

		float distance, distanceA, distanceB, distanceC, beta, betaFinal, Va, tempx, tempy, tempz;

		tempx = (0.0 - camera_pos.x); //0.0
		tempx = tempx * tempx;
		tempy = (0.0 - camera_pos.y); //0.0
		tempy = tempy * tempy;
		tempz = (0.0 - camera_pos.z); //depth
		tempz = tempz * tempz;
		distanceA = tempx + tempy + tempz;
		distanceA = sqrt(distanceA);

		tempx = (farthest.x - camera_pos.x); //0.0
		tempx = tempx * tempx;
		tempy = (farthest.y - camera_pos.y); //0.0
		tempy = tempy * tempy;
		tempz = (farthest.z - camera_pos.z); //depth
		tempz = tempz * tempz;
		distanceB = tempx + tempy + tempz;
		distanceB = sqrt(distanceB);

		tempx = (farthest.x - 0.0); //0.0
		tempx = tempx * tempx;
		tempy = (farthest.y - 0.0); //0.0
		tempy = tempy * tempy;
		tempz = (farthest.z - 0.0); //depth
		tempz = tempz * tempz;
		distanceC = tempx + tempy + tempz;
		distanceC = sqrt(distanceC);

		beta = ((distanceA*distanceA) + (distanceC * distanceC) - (distanceB * distanceB)) / (2 * distanceA*distanceC);
		betaFinal = acos(beta) * 180.0 / M_PI;

		Va = distanceC * (sin(betaFinal*M_PI / 180.0));

		distance = distanceB * distanceB - Va * Va;
		distance = sqrt(distance);

		return distance;
	}

private:
	void init_mesh(const aiScene* scene, const aiMesh* mesh, size_t index);
	bool init_materials(const aiScene* scene, const std::string& filename);

	bool init_from_scene(const aiScene* scene, const std::string& filename) {
		entries.resize(scene->mNumMeshes);
		textures.resize(scene->mNumMaterials);

		for (size_t i = 0; i < entries.size(); ++i) {
			const aiMesh* mesh = scene->mMeshes[i];
			init_mesh(scene, mesh, i);
		}

		return init_materials(scene, filename);
	}

	void clear() {
		for (size_t i = 0; i < textures.size(); ++i) {
			delete textures[i];
			textures[i] = NULL;
		}
	}

#define INVALID_MATERIAL 0xFFFFFFFF

	class MeshEntry {
	public:
		MeshEntry()
			: vb(INVALID_OGL_VALUE),
			ib(INVALID_OGL_VALUE),
			num_indices(0),
			material_index(INVALID_MATERIAL),
			xyz_data(NULL),
			xyz(NULL),
			kdtree(NULL),
			flann_epsilon(0.0),
			flann_sorted(true),
			flann_checks(32)
		{ }

		~MeshEntry() {
			if (vb != INVALID_OGL_VALUE) glDeleteBuffers(1, &vb);
			if (ib != INVALID_OGL_VALUE) glDeleteBuffers(1, &ib);

			delete[] xyz_data;
			delete xyz;
			delete kdtree;
		}

		void init(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices) {
			num_indices = indices.size();

			glGenBuffers(1, &vb);
			glBindBuffer(GL_ARRAY_BUFFER, vb);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

			glGenBuffers(1, &ib);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * num_indices, &indices[0], GL_STATIC_DRAW);

			xyz_data = new float[vertices.size() * 3];
			for (size_t i = 0; i < vertices.size(); ++i) {
				xyz_data[i * 3 + 0] = vertices[i].x();
				xyz_data[i * 3 + 1] = vertices[i].y();
				xyz_data[i * 3 + 2] = vertices[i].z();
			}

			xyz = new flann::Matrix<float>(xyz_data, vertices.size(), 3);

			kdtree = new flann::KDTreeSingleIndex<flann::L2_Simple<float> >(*xyz, flann::KDTreeSingleIndexParams(MAX_LEAF_SIZE));
			kdtree->buildIndex();
		}

		float nearest_point(const glm::vec4& p, glm::vec4& result) const {
			flann::Matrix<float> query(const_cast<float*>(static_cast<const float*>(glm::value_ptr(p))), 1, 3);

			int index;
			float distance;
			flann::Matrix<int> indices(&index, 1, 1);
			flann::Matrix<float> distances(&distance, 1, 1);

			flann::SearchParams search_parameters;
			search_parameters.eps = flann_epsilon;
			search_parameters.sorted = flann_sorted;
			search_parameters.checks = flann_checks;


			kdtree->knnSearch(query, indices, distances, 1, search_parameters);

			result.x = xyz_data[index * 3 + 0];
			result.y = xyz_data[index * 3 + 1];
			result.z = xyz_data[index * 3 + 2];
			result.w = 0.0;

			return distance;
		}

		GLuint vb;
		GLuint ib;
		size_t num_indices;
		size_t material_index;

		float* xyz_data;
		flann::Matrix<float>* xyz;
		flann::KDTreeSingleIndex<flann::L2_Simple<float> >* kdtree;
		float flann_epsilon;
		bool  flann_sorted;
		int   flann_checks;
	};


	std::vector<MeshEntry> entries;
	std::vector<Texture*> textures;
	glm::vec3 min_extremities, max_extremities, centroid_;
};


#endif // MESH_H
