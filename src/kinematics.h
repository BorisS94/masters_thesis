#ifndef KINEMATICS_H
#define KINEMATICS_H

class Kinematics {
public:

	float newJointRotation;
	glm::vec3 EndEffector;
	float currentDistance;
	float newDistance;
	float gradient;

	Kinematics() {

	}

	void inverseKinematics(CAMERA &camera, Scene scene, glm::vec3 Target) {

		for (int i = (ROBOT::getAllObjects().size() - 3); i >= 0; i--) {
			newJointRotation = ROBOT::getAllObjects()[i]->Rotation;
			EndEffector = getEffectorPosition();
			currentDistance = distance(EndEffector, Target);
			newJointRotation += 0.1f;
			ROBOT::getAllObjects()[i]->Rotation += 0.1f;
			scene.render(camera, true);
			EndEffector = getEffectorPosition();
			ROBOT::getAllObjects()[i]->Rotation -= 0.1f;
			newDistance = distance(EndEffector, Target);
			newJointRotation -= 0.1f;
			gradient = (newDistance - currentDistance) / 0.1f;
			newJointRotation = newJointRotation - (0.4f*gradient);
			ROBOT::getAllObjects()[i]->Rotation = newJointRotation;
			scene.render(camera, true);
		}
	}

	glm::vec3 getEffectorPosition() {

		glm::vec3 scale;
		glm::quat rotation;
		glm::vec3 translation;
		glm::vec3 skew;
		glm::vec4 perspective;
		glm::decompose(ROBOT::getAllObjects()[ROBOT::getAllObjects().size() - 2]->Matrix, scale, rotation, translation, skew, perspective);

		return translation * glm::vec3(-1.0, 1.0, -1.0);
	}

	float distance(glm::vec3 x, glm::vec3 y) {

		float distance, tempx, tempy, tempz;
		tempx = (x.x - y.x);
		tempx = tempx * tempx;
		tempy = (x.y - y.y);
		tempy = tempy * tempy;
		tempz = (x.z - y.z);
		tempz = tempz * tempz;
		distance = tempx + tempy + tempz;
		distance = sqrt(distance);

		return distance;
	}


private:

};

#endif // KINEMATICS_H
