#pragma once
#ifndef HEADER_H
#define HEADER_H
#define BOOST_USE_WINDOWS_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <csignal>
#include <limits>
#include <cstdio>
#include <string>
#include <cmath>
#include <thread>
#include <chrono>
#include <random>
#include <experimental/filesystem>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <Eigen/Dense>
#include <Eigen/StdVector>

#include <pcl/io/vtk_lib_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/icp.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/search/kdtree.h>
#include <pcl/surface/poisson.h>
//keypoints
#include <pcl/point_cloud.h>
#include <pcl/common/io.h>
#include <pcl/correspondence.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/board.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/recognition/cg/hough_3d.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/common/transforms.h>
#include <pcl/console/parse.h>


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/matrix_decompose.hpp>


#include "imgui.h"
#include "imgui_impl_glfw_gl3.h"

#include "mesh.h"
#include "camera.h"
#include "robot.h"
#include "icp.h"
#include "scene.h"
#include "kinematics.h"
#include "recon.h"
#include "keypoints.h"

using std::cerr;
using std::cout;
using std::endl;

//multithreading
std::thread t0; //normals
std::thread t1; //registration

//Model name
std::string model_filename;

//kinematics class
Kinematics kinematics;

//registration class
ICP registerICP;

//reconstruction class
RECON recon(false);

//filename
int name;
int name2;
std::string nameS;
std::string nameS2;
std::string filenameTransformation;
std::string filenameCameraPosition;
fstream fileStream;

//camera parameters
CAMERA camera;
CAMERA cameraScanning(glm::vec3(850.0f, 150.0f, 589.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
unsigned int width = 1000, height = 1000, fov = 60;
float lastX = width / 2.0;
float lastY = height / 2.0;
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
bool firstMouse = true;
bool saved_now_quit = false;
std::vector<glm::vec3> CameraPosition;
bool changed = false;
bool pressed = true;

//scanning parameters
std::vector<glm::vec3> ScanningPoints;
int OnePoint = 0;
int ScansDone = 0;
int MaxScans = 0;
int Rotations = 0;
float ScanningRotation = 0.0f;
bool ScanningStart = false;
int Points = 0;
bool done = false;
bool reconstruction = false;
bool registration = false;
bool use_robot = true;
std::string lastNameN;
fstream lastF;

Keypoints key;

#endif // HEADER_H