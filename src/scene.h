#pragma once
#ifndef SCENE_H
#define SCENE_H
#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_RADIANS
#define _USE_MATH_DEFINES

class Scene {
public:

	bool ScanningMode;
	float ModelRotation;
	glm::vec3 TargetPointPosition;
	int PointsReached;
	int RotaReached;
	int CloudDensity;
	int Noise;
	int InitialTransformation;
  float K1, K2, K3, P1, P2;
  int Width, Height;

	Scene(const std::string& filename, bool scanningMode, float modelRotation, glm::vec3 targetPointPosition, int pointsReached, int rotaReached, int cloudDensity, int noise, int initialTransformation, float k1, float k2, float k3, float p1, float p2, int width, int height)
	{
		scannedModel->load_mesh(filename);
		ScanningMode = scanningMode;
		ModelRotation = modelRotation;
		TargetPointPosition = targetPointPosition;
		PointsReached = pointsReached;
		RotaReached = rotaReached;
		CloudDensity = cloudDensity;
		Noise = noise;
		InitialTransformation = initialTransformation;
    K1 = k1;
    K2 = k2;
    K3 = k3;
    P1 = p1;
    P2 = p2;
    Width = width;
    Height = height;

		base->load_mesh("../data/robot/base.stl");
		joint0->load_mesh("../data/robot/joint0.stl");
		joint1->load_mesh("../data/robot/joint1.stl");
		joint2->load_mesh("../data/robot/joint2.stl");
		end_effector->load_mesh("../data/robot/effector.stl");
		target->load_mesh("../data/robot/target.stl");
	}

	void projection_setup(float fov, const glm::mat4& inverse_model, const glm::mat4& view_physics, bool mainMode) {

		gl_setup();

		glm::mat4 inverse_view = glm::inverse(view_physics);
		glm::vec4 camera_pos_mc = inverse_model * inverse_view * glm::vec4(0.0, 0.0, 0.0, 1.0);
		glm::mat4 model = glm::inverse(inverse_model);

		if (ScanningMode == false || mainMode == false) {
			real_near_plane = 0.1f;
			far_plane = 400000.0f;
		}
		else {
			near_plane_bound = scannedModel->near_plane_bound(model, camera_pos_mc);
			real_near_plane = near_plane_bound * NEAR_PLANE_FACTOR;
			far_plane = scannedModel->far_plane_bound(model, camera_pos_mc) * FAR_PLANE_FACTOR;
		}
		projection = glm::perspective<float>(fov * M_PI / 180.0, 1.0, real_near_plane, far_plane);
	}

	void gl_setup() {
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glEnable(GL_NORMALIZE);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_LINE_SMOOTH);
		glDisable(GL_POLYGON_SMOOTH);
		glDisable(GL_MULTISAMPLE);
		glEnable(GL_BLEND);
		glDepthFunc(GL_LEQUAL);
		glFrontFace(GL_CCW);
		glCullFace(GL_BACK);

		glPolygonMode(GL_FRONT, GL_FILL);
	}

	void render(CAMERA &camera, bool Main) {

		camera.ScannerPosition = getEffectorPosition();

		if (ScanningMode == false || Main == false) {
			shader_program = shader_programRegular;
		}
		else {
			shader_program = shader_programModel;
		}
	
		glUseProgram(shader_program->id());

		float light_direction[] = { camera.Position.x, camera.Position.y, camera.Position.z, 0.0 };
		glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light_direction);
		float light_position[] = { camera.Position.x, camera.Position.y, camera.Position.z, 1.0 };
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);

		glm::vec3 EulerAngles(0, glm::radians(ModelRotation), 0);
		glm::dquat Model = glm::quat(EulerAngles);
		glm::mat4 inverse_model_physics = glm::mat4(glm::mat4_cast(Model*flip));
		glm::mat4 inverse_model = glm::scale(glm::mat4(1), glm::vec3(1.0 / 1.0, 1.0 / 1.0, 1.0 / 1.0)) * inverse_model_physics;
		model = glm::inverse(inverse_model);

		GLint far_plane_id = glGetUniformLocation(shader_program->id(), "far_plane");
		GLint near_plane_id = glGetUniformLocation(shader_program->id(), "near_plane");
		GLint v_id = glGetUniformLocation(shader_program->id(), "ViewMatrix");
		GLint mv_id = glGetUniformLocation(shader_program->id(), "ModelViewMatrix");
		GLint normal_id = glGetUniformLocation(shader_program->id(), "NormalMatrix");
		GLint mvp_id = glGetUniformLocation(shader_program->id(), "ModelViewProjectionMatrix");

		glUniform1f(far_plane_id, far_plane);
		glUniform1f(near_plane_id, real_near_plane);
		glm::mat4 view = camera.GetViewMatrix();
		projection_setup(camera.Fov, inverse_model, view, Main);
		glUniformMatrix4fv(v_id, 1, GL_FALSE, &view[0][0]);
		glm::mat4 model_view = view * model;
		glUniformMatrix4fv(mv_id, 1, GL_FALSE, &model_view[0][0]);
		glm::mat3 normal_matrix = glm::inverseTranspose(glm::mat3(model_view));
		glUniformMatrix3fv(normal_id, 1, false, static_cast<GLfloat*>(glm::value_ptr(normal_matrix)));
		glm::mat4 model_view_projection = projection * view * model;
		glUniformMatrix4fv(mvp_id, 1, GL_FALSE, &model_view_projection[0][0]);
		scannedModel->render(shader_program);

		joint0M->setup(shader_programRegular, projection, view, v_id, mv_id, normal_id, mvp_id, joint0);
		joint1M->setup(shader_programRegular, projection, view, v_id, mv_id, normal_id, mvp_id, joint1);
		joint2M->setup(shader_programRegular, projection, view, v_id, mv_id, normal_id, mvp_id, joint2);
		end_effectorM->setup(shader_programRegular, projection, view, v_id, mv_id, normal_id, mvp_id, end_effector);
		baseM->setup(shader_programRegular, projection, view, v_id, mv_id, normal_id, mvp_id, base);

		targetM = glm::inverse(glm::scale(glm::mat4(1), glm::vec3(1.0 / 1.0, 1.0 / 1.0, 1.0 / 1.0)) * glm::mat4(glm::mat4_cast(flip)));
		targetM = glm::translate(targetM, glm::vec3(TargetPointPosition.x, TargetPointPosition.y, TargetPointPosition.z));

		glUseProgram(shader_programRegular->id());
		model_view_projection = projection * view * targetM;
		glUniformMatrix4fv(mvp_id, 1, GL_FALSE, &model_view_projection[0][0]);
		target->render(shader_programRegular);

		glFlush();
	}

	void save_transformation_metadata(const glm::dmat4 test) {

		glm::dmat4 pose = test;

		int name = static_cast<int>(PointsReached);
		std::string nameS = std::to_string(name);
		int name2 = static_cast<int>(RotaReached-1);
		std::string nameS2 = std::to_string(name2);

		std::string filename = "scans//" + nameS + "p" + nameS2 + "r" + ".txt";
		std::ofstream out(filename.c_str());
		out << std::scientific << std::setprecision(std::numeric_limits<double>::digits10 + 1)
			<< pose[0][0] << ' ' << pose[1][0] << ' ' << pose[2][0] << ' ' << pose[3][0] << '\n'
			<< pose[0][1] << ' ' << pose[1][1] << ' ' << pose[2][1] << ' ' << pose[3][1] << '\n'
			<< pose[0][2] << ' ' << pose[1][2] << ' ' << pose[2][2] << ' ' << pose[3][2] << '\n'
			<< pose[0][3] << ' ' << pose[1][3] << ' ' << pose[2][3] << ' ' << pose[3][3] << std::endl;
		out.close();
	}

	void save_transformation_metadataModel(const glm::dquat& model_q) {

		glm::dmat4 get_v_m_w_s = glm::mat4_cast(model_q);

		glm::dmat4 pose = get_v_m_w_s;

		int name = static_cast<int>(PointsReached);
		std::string nameS = std::to_string(name);
		int name2 = static_cast<int>(RotaReached-1);
		std::string nameS2 = std::to_string(name2);

		std::string filename = "scans//" + nameS + "p" + nameS2 + "r" + "model" + ".txt";
		std::ofstream out(filename.c_str());
		out << std::scientific << std::setprecision(std::numeric_limits<double>::digits10 + 1)
			<< pose[0][0] << ' ' << pose[1][0] << ' ' << pose[2][0] << ' ' << pose[3][0] << '\n'
			<< pose[0][1] << ' ' << pose[1][1] << ' ' << pose[2][1] << ' ' << pose[3][1] << '\n'
			<< pose[0][2] << ' ' << pose[1][2] << ' ' << pose[2][2] << ' ' << pose[3][2] << '\n'
			<< pose[0][3] << ' ' << pose[1][3] << ' ' << pose[2][3] << ' ' << pose[3][3] << std::endl;
		out.close();

		std::ofstream outfile("scans//" + nameS + "p" + nameS2 + "r" + "normal" + ".txt");

		outfile << "normals temp" << std::endl;

		outfile.close();
	}

	size_t write_point_cloud(float* data, unsigned int width, unsigned int height) {
		int density;
		glm::ivec4 viewport;
		glm::mat4 identity(1.0);

		glGetIntegerv(GL_VIEWPORT, (int*)&viewport);

		size_t data_count = 0;

		unsigned char *rgba = new unsigned char[4 * width*height + 4];

		glm::mat4 axis_flip = glm::scale(glm::mat4(1.0), glm::vec3(-1.0, 1.0, -1.0));

		glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)(rgba));
		
		if (CloudDensity == 5)
			density = 1;
		if (CloudDensity == 4)
			density = 2;
		if (CloudDensity == 3)
			density = 3;
		if (CloudDensity == 2)
			density = 4;
		if (CloudDensity == 1)
			density = 5;

		for (size_t i = 0; i < height; i = i = i+density) {
			for (size_t j = 0; j < width; j = j = j+density) {
				size_t pos = 4 * (j*height + i);

				int gb = rgba[pos + 1] * 255 + rgba[pos + 2];
				if (gb == 0) continue;

				double t = gb / 65536.0;
				double d = t * (far_plane - real_near_plane) + real_near_plane;

				//float winZ;
				//glReadPixels(i, j, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

				glm::vec3 win(i, j, t);
				glm::vec4 position(glm::unProject(win, identity, projection, viewport), 0.0);

				//position.z = winZ * (far_plane - real_near_plane) + real_near_plane;
				position.z = -d;
				glm::vec4 position_cc = axis_flip * position;

				data[data_count] = position_cc[0];
				data[data_count + 1] = position_cc[1];
				data[data_count + 2] = position_cc[2];
				data[data_count + 3] = rgba[pos + 0] / 256.0;

				data_count += 4;
			}
		}

		return data_count;
	}

	void save_PCD_point_cloud(unsigned int width, unsigned int height, CAMERA camera) {

		int name = static_cast<int>(PointsReached);
		std::string nameS = std::to_string(name);
		int name2 = static_cast<int>(RotaReached-1);
		std::string nameS2 = std::to_string(name2);
		std::string filename = "scans//" + nameS + "p" + nameS2 + "r" + ".pcd";

		std::cerr << "Saving point cloud..." << std::endl;

		float* data = new float[4 * width*height + 4];
		size_t data_count = write_point_cloud(data, width, height);

		std::ofstream out(filename, std::ios_base::binary | std::ios_base::out);

		// Print PCD header
		out << "VERSION .7\nFIELDS x y z intensity\nSIZE 4 4 4 4\nTYPE F F F F\nCOUNT 1 1 1 1\n";
		out << "WIDTH " << data_count / 4 << std::endl;
		out << "HEIGHT " << 1 << std::endl;

		out << "VIEWPOINT 0 0 0 1 0 0 0" << std::endl;
		out << "POINTS " << data_count / 4 << std::endl;
		out << "DATA binary" << std::endl;
		out.write((char*)(data), sizeof(float)*data_count);

		out.close();
		
		std::cerr << "Saved '" << filename << "'" << std::endl;

		glm::vec3 EulerAngles(0, glm::radians(ModelRotation), 0);
		glm::dquat modelTransformation = glm::dquat(EulerAngles);

		glm::mat4 view = camera.GetViewMatrix();
		glm::dquat rotation = glm::quat_cast(view);
		glm::dmat4 inverseModelView = glm::inverse(glm::lookAt(getModelPosition(), camera.Position*glm::vec3(-1.0, 1.0, -1.0), camera.Up));
		save_transformation_metadata(inverseModelView);
		save_transformation_metadataModel(modelTransformation);

		//noise
		float noise_std = 0.0f;
		if (Noise == 5)
			noise_std = 0.5f;
		if (Noise == 4)
			noise_std = 0.4f;
		if (Noise == 3)
			noise_std = 0.3f;
		if (Noise == 2)
			noise_std = 0.2f;
		if (Noise == 1)
			noise_std = 0.1f;
		if (Noise == 0)
			noise_std = 0.0f;

		std::random_device rd;
		std::mt19937 rng(rd());
		std::normal_distribution<float> nd(0.0f, noise_std * noise_std);

		pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
		pcl::io::loadPCDFile(filename, *cloud);

		for (auto& point : cloud->points)
		{
			// Gaussian
			point.x += nd(rng); point.y += nd(rng); point.z += nd(rng);
		}

    for (auto& point : cloud->points)
    {
      float focal_lengthW = Width / (2 * std::tan(camera.Fov / 2));
      float focal_lengthH = Height / (2 * std::tan(camera.Fov / 2));

      point.x = (point.x - Width/2) / (focal_lengthW*5);
      point.y = (point.y - Height/2) / (focal_lengthH*5);

      // Distortion
      double R2 = point.x * point.x + point.y * point.y;

      // Radial distorsion
      point.x = point.x * (1 + K1 / 80 * R2 + K2 / 80 * R2 * R2 + K3 / 80 * R2 * R2 * R2);
      point.y = point.y * (1 + K1 / 80 * R2 + K2 / 80 * R2 * R2 + K3 / 80 * R2 * R2 * R2);

      // Tangential distorsion
      point.x = point.x + (2 * P1 / 50 * point.x * point.y + P2 / 50 * (R2 + 2 * point.x * point.x));
      point.y = point.y + (P1 / 50 * (R2 + 2 * point.y * point.y) + 2 * P2 / 50 * point.x * point.y);

      point.x = (point.x * (focal_lengthW*5)) + Width/2;
      point.y = (point.y * (focal_lengthH*5)) + Height/2;
      
    }
		//pcl::io::savePCDFileASCII(filename, *cloud);

		pcl::PCDWriter write;
		write.writeBinaryCompressed(filename, *cloud);

	}

	glm::vec3 getModelPosition() {

		glm::vec3 scale;
		glm::quat rotation;
		glm::vec3 translation;
		glm::vec3 skew;
		glm::vec4 perspective;
		glm::decompose(model, scale, rotation, translation, skew, perspective);

		return translation * glm::vec3(-1.0, 1.0, -1.0);
	}

	glm::vec3 getEffectorPosition() {

		glm::vec3 scale;
		glm::quat rotation;
		glm::vec3 translation;
		glm::vec3 skew;
		glm::vec4 perspective;
		glm::decompose(end_effectorM->Matrix, scale, rotation, translation, skew, perspective);

		return translation * glm::vec3(-1.0, 1.0, -1.0);
	}

	glm::vec3 getTargetPosition() {

		glm::vec3 scale;
		glm::quat rotation;
		glm::vec3 translation;
		glm::vec3 skew;
		glm::vec4 perspective;
		glm::decompose(targetM, scale, rotation, translation, skew, perspective);

		return translation * glm::vec3(-1.0, 1.0, -1.0);
	}

private:

	const float NEAR_PLANE_FACTOR = 0.99f;
	const float FAR_PLANE_FACTOR = 1.01f;

	Shader* shader_program = new Shader("../data/shader/spotv.glsl", "../data/shader/fragScanner.glsl");
	Shader* shader_programModel = new Shader("../data/shader/spotv.glsl", "../data/shader/fragScanner.glsl");
	Shader* shader_programRegular = new Shader("../data/shader/spotv.glsl", "../data/shader/fragRegular.glsl");

	glm::dquat flip = glm::angleAxis<double>(M_PI, glm::dvec3(0.0, 1.0, 0.0));
	float scale_factor;
	glm::mat4 projection;

	GLfloat near_plane_bound;
	GLfloat real_near_plane;
	GLfloat far_plane;

	//Model and target
	Mesh* scannedModel = new Mesh();
	Mesh* target = new Mesh();
	glm::mat4 model;
	glm::mat4 targetM;

	//ROBOT
	Mesh* base = new Mesh();
	Mesh* joint0 = new Mesh();
	Mesh* joint1 = new Mesh();
	Mesh* joint2 = new Mesh();
	Mesh* end_effector = new Mesh();
	//base needs to be defined last and effector right before
	ROBOT *joint0M = new ROBOT(glm::vec3(0.0f, -27.0f, -700.0f), glm::vec3(36.0f, 156.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, false, false, 0);
	ROBOT *joint1M = new ROBOT(joint0M->Lenght, glm::vec3(0.0f, 254.8f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), -35.0f, false, false, 1);
	ROBOT *joint2M = new ROBOT(joint1M->Lenght, glm::vec3(0.0f, 228.0f, 20.0f), glm::vec3(1.0f, 0.0f, 0.0f), 85.0f, false, false, 2);
	ROBOT *end_effectorM = new ROBOT(joint2M->Lenght, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f, false, true, 3);
	ROBOT *baseM = new ROBOT(glm::vec3(0.0f, -27.0f, -700.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.0f, true, false, 4);
};

#endif
