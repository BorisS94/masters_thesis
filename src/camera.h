#pragma once
#ifndef CAMERA_H
#define CAMERA_H

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

// Default camera values
const float YAW = -90.0f;
const float PITCH = 0.0f;
const float SPEED = 5.5f;
const float SENSITIVITY = 0.6f;

class CAMERA {

private:
	bool TempPosition = false;
	glm::vec3 TempCameraPosition;

public:

	// Camera Attributes
	glm::vec3 Position;
	glm::vec3 Front;
	glm::vec3 FinalFront;
	glm::vec3 Up;
	glm::vec3 ScannerPosition;
	// Euler Angles
	float Yaw;
	float Pitch;
	// Camera options
	float MovementSpeed;
	float MouseSensitivity;
	float Fov;
	bool MouseRotation;
	bool ScanningView;

	CAMERA(glm::vec3 position = glm::vec3(0.0f, 0.0f, 500.0f), glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH, float fov = 60.0f, bool scanningView = false, bool mouseRotation = false) : MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY) {
		Position = position;
		Front = front;
		FinalFront = position + front;
		Up = up;
		Yaw = yaw;
		Pitch = pitch;
		Fov = fov;
		ScanningView = scanningView;
		MouseRotation = mouseRotation;
	}

	// Returns the view matrix calculated using Euler Angles and the LookAt Matrix
	glm::mat4 GetViewMatrix()
	{
		if (ScanningView) {
			if (!TempPosition) {
				TempCameraPosition = Position;
				TempPosition = true;
			}
			Position = ScannerPosition*glm::vec3(-1.0, 1.0, -1.0);
			FinalFront = glm::vec3(0.0f,0.0f,0.0f)*glm::vec3(-1.0, 1.0, -1.0);
		}

		else {
			if (TempPosition) {
				Position = TempCameraPosition;
				TempPosition = false;
			}

			TempCameraPosition = Position;
			FinalFront = Front + Position;
		}
		
		return glm::lookAt(Position, FinalFront, Up);
	}

	glm::mat4 GetInverseViewMatrix()
	{
		if(ScanningView) {
			if (!TempPosition) {
				TempCameraPosition = Position;
				TempPosition = true;
			}
			Position = ScannerPosition * glm::vec3(-1.0, 1.0, -1.0);
			FinalFront = glm::vec3(0.0f, 0.0f, 0.0f)*glm::vec3(-1.0, 1.0, -1.0);
		}

		else {
		if (TempPosition) {
			Position = TempCameraPosition;
			TempPosition = false;
		}

		TempCameraPosition = Position;
		FinalFront = Front + Position;
		}

		return glm::inverse(glm::lookAt(Position, FinalFront, Up));
	}

	void UpdateCamera(float xoffset, float yoffset) {
		Yaw += xoffset;
		Pitch += yoffset;

		if (Pitch > 89.0f)
			Pitch = 89.0f;
		if (Pitch < -89.0f)
			Pitch = -89.0f;

		Front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
		Front.y = sin(glm::radians(Pitch));
		Front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
	}

	// Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
	void ProcessKeyboard(Camera_Movement direction, float deltaTime)
	{
		if (direction == FORWARD)
			Position += MovementSpeed * deltaTime * Front;
		if (direction == BACKWARD)
			Position -= MovementSpeed * deltaTime * Front;
		if (direction == LEFT)
			Position -= glm::normalize(glm::cross(Front, Up)) * (MovementSpeed * deltaTime);
		if (direction == RIGHT)
			Position += glm::normalize(glm::cross(Front, Up)) * (MovementSpeed * deltaTime);
	}

};

#endif // CAMERA_H

