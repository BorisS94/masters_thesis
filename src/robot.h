#pragma once
#ifndef ROBOT_H
#define ROBOT_H

class ROBOT {
public:

	glm::mat4 Matrix;
	glm::vec3 Position;
	glm::vec3 Lenght;
	glm::vec3 Axis;
	float Rotation;
	bool IsBase;
	bool IsEffector;
	int Number;

	ROBOT(glm::vec3 position, glm::vec3 lenght, glm::vec3 axis, float rotation, bool isBase, bool isEffector, int number) {
		Position = position;
		Lenght = lenght;
		Axis = axis;
		Rotation = rotation;
		IsBase = isBase;
		IsEffector = isEffector;
		Number = number;
		objList.push_back(this);
	}

	static std::vector<ROBOT*> getAllObjects() {
		return objList;
	}

	void setup(Shader* shader, glm::mat4 projection, glm::mat4 view, GLint v_id, GLint mv_id, GLint normal_id, GLint mvp_id, Mesh *part) {
		
		Matrix = glm::inverse(glm::scale(glm::mat4(1), glm::vec3(1.0 / 1.0, 1.0 / 1.0, 1.0 / 1.0)) * glm::mat4(glm::mat4_cast(flip)));
		
		if (!IsBase) {
			for (int i = 0; i < Number; i++) {
				Matrix = glm::translate(Matrix, ROBOT::getAllObjects()[i]->Position);
				Matrix = glm::rotate(Matrix, glm::radians(ROBOT::getAllObjects()[i]->Rotation), ROBOT::getAllObjects()[i]->Axis);
			}
		}

		if (IsEffector) {
			Matrix = glm::translate(Matrix, Position);
		}
		else {
			Matrix = glm::translate(Matrix, Position);
			Matrix = glm::rotate(Matrix, glm::radians(Rotation), Axis);
		}

		glUseProgram(shader->id());

		glUniformMatrix4fv(v_id, 1, GL_FALSE, &view[0][0]);
		glm::mat4 model_view = view * Matrix;
		glUniformMatrix4fv(mv_id, 1, GL_FALSE, &model_view[0][0]);
		glm::mat3 normal_matrix = glm::inverseTranspose(glm::mat3(model_view));
		glUniformMatrix3fv(normal_id, 1, false, static_cast<GLfloat*>(glm::value_ptr(normal_matrix)));
		glm::mat4 model_view_projection = projection * view * Matrix;
		glUniformMatrix4fv(mvp_id, 1, GL_FALSE, &model_view_projection[0][0]);
		part->render(shader);
	}


private:
	static std::vector<ROBOT*> objList;
	glm::dquat flip = glm::angleAxis<double>(M_PI, glm::dvec3(0.0, 1.0, 0.0));

};

std::vector<ROBOT*> ROBOT::objList;

#endif // ROBOT_H

