#pragma once
#ifndef RECON_H
#define RECON_H

class RECON {
public:

  bool OutlierRemoval;

	RECON(bool outlierRemoval) {
    OutlierRemoval = outlierRemoval;
  }
  
/*glm::vec3 ComputeNormal(std::vector<glm::vec3> points)
{
  int realn = 0;
  glm::vec3 centroid = glm::vec3(0, 0, 0);
  glm::vec3 normal = glm::vec3(0, 0, 0);

  for (unsigned int i = 0; i < points.size(); i++)
  {
    realn++;
    centroid.x += points[i].x;
    centroid.y += points[i].y;
    centroid.z += points[i].z;
  }

  const float realn_fract = static_cast<float>(1.0) / static_cast<float>(realn);

  centroid.x *= realn_fract;
  centroid.y *= realn_fract;
  centroid.z *= realn_fract;

  float a[9];
  for (int i = 0; i < 9; i++) { a[i] = 0; }

  for (unsigned int i = 0; i < points.size(); i++)
  {
    a[0] += glm::pow(points[i].x - centroid.x, 2);
    a[1] += (points[i].x - centroid.x) * (points[i].y - centroid.y);
    a[2] += (points[i].x - centroid.x) * (points[i].z - centroid.z);
    a[3] += (points[i].x - centroid.x) * (points[i].y - centroid.y);
    a[4] += glm::pow(points[i].y - centroid.y, 2);
    a[5] += (points[i].y - centroid.y) * (points[i].z - centroid.z);
    a[6] += (points[i].x - centroid.x) * (points[i].z - centroid.z);
    a[7] += (points[i].y - centroid.y) * (points[i].z - centroid.z);
    a[8] += glm::pow(points[i].z - centroid.z, 2);
  }

  cv::Mat A = cv::Mat(3, 3, cv::DataType<float>::type, &a[0]);

  cv::SVD SingularValueDecomposition(A);

  normal.x = SingularValueDecomposition.u.ptr<float>()[2];
  normal.y = SingularValueDecomposition.u.ptr<float>()[5];
  normal.z = SingularValueDecomposition.u.ptr<float>()[8];

  float distance = glm::sqrt(glm::pow(normal.x, 2) + glm::pow(normal.y, 2) + glm::pow(normal.z, 2));

  const float dist_fract = static_cast<float>(1.0) / distance;

  normal.x *= dist_fract;
  normal.y *= dist_fract;
  normal.z *= dist_fract;

  return normal;
}

std::vector<glm::vec3> NormalEstimation(struct PointCloud cloud)
{
  std::vector<glm::vec3> normals;

  const uint32_t radius = 3;
  const float radius_threshold = 1.0f * radius;
  const float radius_threshold_sqr = radius_threshold * radius_threshold;

  std::vector<glm::vec3> local_points;

  for (uint32_t h = 0; h < cloud.height; h++)
  {
    for (uint32_t w = 0; w < cloud.width; w++)
    {
      glm::vec3 normal = glm::vec3(0, 0, 0);

      if ((radius - 1) < h && h < (cloud.height - radius) && (radius - 1) < w && w < (cloud.width - radius))
      {
        const auto middle_point = cloud.points[(h * cloud.width) + w];

        local_points.clear();
        local_points.reserve(radius * radius);

        for (uint32_t h2 = h - radius; h2 < h + radius; h2++)
        {
          for (uint32_t w2 = w - radius; w2 < w + radius; w2++)
          {
            const auto inspected_point = cloud.points[(h2 * cloud.width) + w2];

            const glm::vec3 diff = inspected_point - middle_point;

            if (glm::dot(diff, diff) <= radius_threshold_sqr)
            {
              local_points.emplace_back(inspected_point);
            }
          }
        }
        normal = ComputeNormal(local_points);

        glm::vec3 to_sensor = glm::vec3(middle_point - cloud.camera_pos);

        if (glm::dot(to_sensor, normal) > 0.0) { normal *= -1; }

        normals.push_back(glm::vec3(normal.x, normal.y, normal.z));
      }

      else
      {
        normal = glm::vec3(0, 0, 0);
        normals.push_back(glm::vec3(normal.x, normal.y, normal.z));
      }
    }
  }
  return normals;
}*/

	void normalsAllClouds(int numberOfPoints, int numberOfRotations) {
		int count = 0;
		for (int i = 0; i <= numberOfRotations; i++) {
			for (int j = 0; j < numberOfPoints; j++) {
				int name = static_cast<int>(j);
				std::string nameS = std::to_string(name);
				int name2 = static_cast<int>(i);
				std::string nameS2 = std::to_string(name2);
				std::string filename = nameS + "p" + nameS2 + "r" + ".pcd";
				doNormals(filename);
				count++;
			}
		}
		printf("normals done");
	}

	void doNormals(std::string model_filename) {
		// load point cloud
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
		pcl::io::loadPCDFile("scans//" + model_filename, *cloud);

		pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
		ne.setInputCloud(cloud);

		// Create an empty kdtree representation, and pass it to the normal estimation object.
		// Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
		pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
		ne.setSearchMethod(tree);

		// Output datasets
		pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);

		ne.setKSearch(100);

		// Compute the features
		ne.compute(*normals);

		pcl::copyPointCloud(*cloud, *cloud_with_normals);
		pcl::copyPointCloud(*normals, *cloud_with_normals);

		pcl::io::savePCDFileASCII("scans//" + model_filename, *cloud_with_normals);


	}

	void doNormalsParalel(int numberOfPoints, int numberOfRotations) {

		std::this_thread::sleep_for(std::chrono::milliseconds(1000));

		for (int i = 0; i <= numberOfRotations; i++) {
			for (int j = 0; j < numberOfPoints; j++) {

				int name = static_cast<int>(j);
				std::string nameS = std::to_string(name);
				int name2 = static_cast<int>(i);
				std::string nameS2 = std::to_string(name2);
				std::string filename = "scans//" + nameS + "p" + nameS2 + "r";
				std::string model_filename = filename + ".pcd";

				std::string fileStreamTxtNormalsS = filename + "normal" + ".txt";;
				fstream fileStreamTxtNormals;
				fileStreamTxtNormals.open(fileStreamTxtNormalsS);

				if (!fileStreamTxtNormals.fail()) {
					
					// load point cloud
					pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
					pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals(new pcl::PointCloud<pcl::PointNormal>);
					pcl::io::loadPCDFile(model_filename, *cloud);

          if (OutlierRemoval) {
            //REMOVE OUTLIERS
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>);
            pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
            sor.setInputCloud(cloud);
            sor.setMeanK(20);
            sor.setStddevMulThresh(1.0);
            sor.filter(*cloud_filtered);
          }

					pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
					ne.setInputCloud(cloud);

					// Create an empty kdtree representation, and pass it to the normal estimation object.
					// Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
					pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>());
					ne.setSearchMethod(tree);

					// Output datasets
					pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);

					ne.setKSearch(100);

					// Compute the features
					ne.compute(*normals);

					pcl::copyPointCloud(*cloud, *cloud_with_normals);
					pcl::copyPointCloud(*normals, *cloud_with_normals);

					//pcl::io::savePCDFileASCII(filename + "Normals" + ".pcd", *cloud_with_normals);
					pcl::PCDWriter write;
					write.writeBinaryCompressed(filename + "Normals" + ".pcd", *cloud_with_normals);

					fileStreamTxtNormals.close();
					remove(fileStreamTxtNormalsS.c_str());
					maxNormals++;

					std::fstream myfile1(filename + "CameraPosition" + ".txt", std::ios_base::in);
					float x,y,z;
					myfile1 >> x >> y >> z;
					glm::vec3 camPos;
					camPos.x = x;
					camPos.y = y;
					camPos.z = z;

					registerClouds.doOneRegistration(filename  + "Normals" + ".pcd", model_filename, camPos, maxNormals);

					printf("registration done %d/%d\n", maxNormals, numberOfPoints*(numberOfRotations + 1));
				}
			}
		}
		if (maxNormals != (numberOfPoints*(numberOfRotations + 1))) {
			doNormalsParalel(numberOfPoints, numberOfRotations);
		}
		/*else {
			std::ofstream outfile("normalsDone.txt");

			outfile << "normals temp" << std::endl;

			outfile.close();
		}*/
	}

	void reconstruct() {

		pcl::PointCloud<pcl::PointNormal>::Ptr cloud(new pcl::PointCloud<pcl::PointNormal>);
		
		pcl::io::loadPCDFile("scans//00.pcd", *cloud);
		
		pcl::io::savePLYFileASCII("scans//00.ply", *cloud);

		pcl::io::loadPLYFile("scans//00.ply", *cloud);

		pcl::Poisson<pcl::PointNormal> poisson;
		poisson.setDepth(9);
		poisson.setSamplesPerNode(1.5);
		poisson.setInputCloud(cloud);

		pcl::PolygonMesh mesh;
		poisson.reconstruct(mesh);

		pcl::io::savePLYFile("mesh.ply", mesh);
	}

private:

	int maxNormals = 0;
	ICP registerClouds;

};

#endif // RECON_H

