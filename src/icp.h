#pragma once
#ifndef ICP_H
#define ICP_H

class ICP {
public:

	ICP() {

	}

	void registerCloud(int numberOfPoints, int numberOfRotations, std::vector<glm::vec3> CameraPosition) {
		int count = 0;
		for (int i = 0; i <= numberOfRotations; i++) {
			for (int j = 0; j < numberOfPoints; j++) {
				int name = static_cast<int>(j);
				std::string nameS = std::to_string(name);
				int name2 = static_cast<int>(i);
				std::string nameS2 = std::to_string(name2);
				std::string filename = nameS + "p" + nameS2 + "r" + "Normals" + ".pcd";
				std::string transformFilename = nameS + "p" + nameS2 + "r" + ".pcd";
				doRegistration(filename, transformFilename, CameraPosition[count], count, (numberOfPoints*(numberOfRotations + 1)));
				count++;
			}
		}
		printf("registration done, ready for reconstruction\n");
	}

	void doRegistration(std::string model_filename, std::string transform_filename, glm::vec3& cameraPos, int count, int max) {

		pcl::PointCloud<pcl::PointNormal>::Ptr cervenyZarovnanyZ(new pcl::PointCloud<pcl::PointNormal>);  // ICP output point cloud
		pcl::PointCloud<pcl::PointNormal>::Ptr finalI(new pcl::PointCloud<pcl::PointNormal>);  // final
		int iterations = 50;

		pcl::io::loadPCDFile(model_filename, *cervenyZarovnanyZ);

		//TRANSFORMACIA
		std::string transform1, transform2;

		transform1 = transform_filename.substr(0, transform_filename.find("."));

		transform2 = transform_filename.substr(0, transform_filename.find("."));

		std::fstream myfile1("scans//" + transform1 + ".txt", std::ios_base::in);

		std::fstream myfile2("scans//" + transform2 + "model" + ".txt", std::ios_base::in);

		float a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;

		myfile1 >> a >> b >> c >> d >> e >> f >> g >> h >> i >> j >> k >> l >> m >> n >> o >> p;

		Eigen::Matrix4f t = Eigen::Matrix4f::Identity();
		Eigen::Matrix4f t2 = Eigen::Matrix4f::Identity();

		t(0, 0) = a;
		t(0, 1) = b;
		t(0, 2) = c;
		t(0, 3) = d;

		t(1, 0) = e;
		t(1, 1) = f;
		t(1, 2) = g;
		t(1, 3) = h;

		t(2, 0) = i;
		t(2, 1) = j;
		t(2, 2) = k;
		t(2, 3) = l;

		t(3, 0) = m;
		t(3, 1) = n;
		t(3, 2) = o;
		t(3, 3) = p;

		float distance, distanceA, distanceB, distanceC, beta, betaFinal, Va, tempx, tempy, tempz;

		tempx = (0.0 - cameraPos.x); 
		tempx = tempx * tempx;
		tempy = (0.0 - cameraPos.y); 
		tempy = tempy * tempy;
		tempz = (0.0 - cameraPos.z); 
		tempz = tempz * tempz;
		distanceA = tempx + tempy + tempz;
		distanceA = sqrt(distanceA);

		pcl::transformPointCloudWithNormals(*cervenyZarovnanyZ, *cervenyZarovnanyZ, t);

		myfile2 >> a >> b >> c >> d >> e >> f >> g >> h >> i >> j >> k >> l >> m >> n >> o >> p;

		t2(0, 0) = a;
		t2(0, 1) = b;
		t2(0, 2) = c;
		t2(0, 3) = d;

		t2(1, 0) = e;
		t2(1, 1) = f;
		t2(1, 2) = g;
		t2(1, 3) = h;

		t2(2, 0) = i;
		t2(2, 1) = j;
		t2(2, 2) = k;
		t2(2, 3) = l;

		t2(3, 0) = m;
		t2(3, 1) = n;
		t2(3, 2) = o;
		t2(3, 3) = p;

		pcl::transformPointCloudWithNormals(*cervenyZarovnanyZ, *cervenyZarovnanyZ, t2);

		pcl::PointCloud<pcl::PointNormal>::Ptr point1(new pcl::PointCloud<pcl::PointNormal>);
		point1->width = 1;
		point1->height = 1;
		point1->is_dense = false;
		point1->points.resize(point1->width * point1->height);
		point1->points[0].x = 0.0;
		point1->points[0].y = 0.0;
		point1->points[0].z = distanceA;

		pcl::transformPointCloud(*point1, *point1, t);
		pcl::transformPointCloud(*point1, *point1, t2);

		Eigen::Matrix4f tPoint = Eigen::Matrix4f::Identity();

		tPoint(0, 3) = -point1->points[0].x;
		tPoint(1, 3) = -point1->points[0].y;
		tPoint(2, 3) = -point1->points[0].z + 300; 

		pcl::transformPointCloudWithNormals(*cervenyZarovnanyZ, *cervenyZarovnanyZ, tPoint);

		pcl::io::savePCDFileASCII(model_filename, *cervenyZarovnanyZ);

		doICP("scans//00.pcd", model_filename);

		printf("registered clouds %d/%d\n", count+1, max);
	}

	void doOneRegistration(std::string model_filename, std::string transform_filename, glm::vec3& cameraPos, int count) {

		pcl::PointCloud<pcl::PointNormal>::Ptr cervenyZarovnanyZ(new pcl::PointCloud<pcl::PointNormal>);  // ICP output point cloud
		pcl::PointCloud<pcl::PointNormal>::Ptr finalI(new pcl::PointCloud<pcl::PointNormal>);  // final
		int iterations = 50;
		
		pcl::io::loadPCDFile(model_filename, *cervenyZarovnanyZ);

		//TRANSFORMACIA
		std::string transform1, transform2;

		transform1 = transform_filename.substr(0, transform_filename.find("."));

		transform2 = transform_filename.substr(0, transform_filename.find("."));

		std::fstream myfile1(transform1 + ".txt", std::ios_base::in);

		std::fstream myfile2(transform2 + "model" + ".txt", std::ios_base::in);

		float a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;

		myfile1 >> a >> b >> c >> d >> e >> f >> g >> h >> i >> j >> k >> l >> m >> n >> o >> p;

		Eigen::Matrix4f t = Eigen::Matrix4f::Identity();
		Eigen::Matrix4f t2 = Eigen::Matrix4f::Identity();

    //camera
		t(0, 0) = a;
		t(0, 1) = b;
		t(0, 2) = c;
		t(0, 3) = d;

		t(1, 0) = e;
		t(1, 1) = f;
		t(1, 2) = g;
		t(1, 3) = h;

		t(2, 0) = i;
		t(2, 1) = j;
		t(2, 2) = k;
		t(2, 3) = l;

		t(3, 0) = m;
		t(3, 1) = n;
		t(3, 2) = o;
		t(3, 3) = p;

		float distance, distanceA, distanceB, distanceC, beta, betaFinal, Va, tempx, tempy, tempz;

		tempx = (0.0 - cameraPos.x);
		tempx = tempx * tempx;
		tempy = (0.0 - cameraPos.y);
		tempy = tempy * tempy;
		tempz = (0.0 - cameraPos.z);
		tempz = tempz * tempz;
		distanceA = tempx + tempy + tempz;
		distanceA = sqrt(distanceA);

		pcl::transformPointCloudWithNormals(*cervenyZarovnanyZ, *cervenyZarovnanyZ, t);

		myfile2 >> a >> b >> c >> d >> e >> f >> g >> h >> i >> j >> k >> l >> m >> n >> o >> p;

    //model
		t2(0, 0) = a;
		t2(0, 1) = b;
		t2(0, 2) = c;
		t2(0, 3) = d;

		t2(1, 0) = e;
		t2(1, 1) = f;
		t2(1, 2) = g;
		t2(1, 3) = h;

		t2(2, 0) = i;
		t2(2, 1) = j;
		t2(2, 2) = k;
		t2(2, 3) = l;

		t2(3, 0) = m;
		t2(3, 1) = n;
		t2(3, 2) = o;
		t2(3, 3) = p;

		pcl::transformPointCloudWithNormals(*cervenyZarovnanyZ, *cervenyZarovnanyZ, t2);

		pcl::PointCloud<pcl::PointNormal>::Ptr point1(new pcl::PointCloud<pcl::PointNormal>);
		point1->width = 1;
		point1->height = 1;
		point1->is_dense = false;
		point1->points.resize(point1->width * point1->height);
		point1->points[0].x = 0.0;
		point1->points[0].y = 0.0;
		point1->points[0].z = distanceA;

		pcl::transformPointCloud(*point1, *point1, t);
		pcl::transformPointCloud(*point1, *point1, t2);

		Eigen::Matrix4f tPoint = Eigen::Matrix4f::Identity();

		tPoint(0, 3) = -point1->points[0].x;
		tPoint(1, 3) = -point1->points[0].y;
		tPoint(2, 3) = -point1->points[0].z + 300;

		pcl::transformPointCloudWithNormals(*cervenyZarovnanyZ, *cervenyZarovnanyZ, tPoint); //podla pozicie

		//pcl::io::savePCDFileASCII(model_filename, *cervenyZarovnanyZ);

		Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
		transform(0, 0) = transform(0, 0);
		transform(1, 1) = transform(1, 1);
		transform(2, 2) = transform(2, 2);
		pcl::transformPointCloud(*cervenyZarovnanyZ, *cervenyZarovnanyZ, transform);

		pcl::PCDWriter write;
		write.writeBinaryCompressed(model_filename, *cervenyZarovnanyZ);
    write.writeBinaryCompressed("vizu//" + std::to_string(count) + ".pcd", *cervenyZarovnanyZ);

		doICP("scans//00.pcd", model_filename);

	}

	void doICP(std::string model_filename1, std::string model_filename2) {

		pcl::PointCloud<pcl::PointNormal>::Ptr prvyBiely(new pcl::PointCloud<pcl::PointNormal>);  // Original point cloud
		pcl::PointCloud<pcl::PointNormal>::Ptr druhyZeleny(new pcl::PointCloud<pcl::PointNormal>);  // Transformed point cloud
		pcl::PointCloud<pcl::PointNormal>::Ptr cervenyZarovnanyZ(new pcl::PointCloud<pcl::PointNormal>);  // ICP output point cloud
		pcl::PointCloud<pcl::PointNormal>::Ptr finalI(new pcl::PointCloud<pcl::PointNormal>);  // final
		int iterations = 50;

		pcl::io::loadPCDFile(model_filename1, *prvyBiely);
		pcl::io::loadPCDFile(model_filename2, *cervenyZarovnanyZ);

		*druhyZeleny = *cervenyZarovnanyZ;  

		// The Iterative Closest Point algorithm
		pcl::IterativeClosestPoint<pcl::PointNormal, pcl::PointNormal> icp;
		icp.setMaximumIterations(iterations);
		icp.setTransformationEpsilon(1e-9);
		icp.setMaxCorrespondenceDistance(0.4);
		icp.setEuclideanFitnessEpsilon(1);
		icp.setRANSACOutlierRejectionThreshold(0.01);
		icp.setInputSource(cervenyZarovnanyZ);
		icp.setInputTarget(prvyBiely);
		icp.align(*cervenyZarovnanyZ);

		*finalI = *cervenyZarovnanyZ + *prvyBiely;

		//pcl::io::savePCDFileASCII("00.pcd", *finalI);

		pcl::PCDWriter write;
		write.writeBinaryCompressed("scans//00.pcd", *finalI);
	}

private:

};

#endif // ICP_H

